export const isDivisibleBy = ({ number, divider }) => {
  return number % divider == 0;
};
