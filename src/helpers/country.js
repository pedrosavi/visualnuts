export const getNumberOfCountries = (countries) => {
  return countries.length;
}

export const getCountryWithMoreLanguages = (countries) => {
  const countryWithMoreLanguages = countries.reduce((highestCountry, country) => {
    if (hasMoreLanguagesThanHighest(highestCountry, country)) {
      highestCountry = country;
    }

    return highestCountry
  }, countries[0]);

  return countryWithMoreLanguages;
}

export const getMostCommonLanguage = (countries) => {
  const languages = getLanguagesList(countries);
  const sortedLanguages = getLanguagesSortedByCountry(languages);
  const highestLanguage = sortedLanguages[0];
  const highestLanguageList = getOtherLanguagesWithEqualHighest({ sortedLanguages, highestLanguage });

  return highestLanguageList;
}

const hasMoreLanguagesThanHighest = (highestCountry, country) => {
  return highestCountry.languages.length < country.languages.length;
}

const getLanguagesList = (countries) => {
  let languages = {};

  for (let i = 0; i < countries.length; i++) {
    for (let j = 0; j < countries[i].languages.length; j++) {
      const lang = countries[i].languages[j];

      if (languages[lang]) {
        languages[lang].countries++;
      } else {
        languages[lang] = { countries: 1 };
      }
    }
  }

  return languages;
}

const getLanguagesSortedByCountry = (languages) => {
  const sortedList = Object.keys(languages).map(key => {
    return {
      language: key,
      countries: languages[key].countries
    };
  }).sort((a, b) => b.countries - a.countries)

  return sortedList;
}

const getOtherLanguagesWithEqualHighest = ({ sortedLanguages, highestLanguage }) => {
  return sortedLanguages.filter(language =>
    language.countries == highestLanguage.countries);
}
