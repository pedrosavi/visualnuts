import { isDivisibleBy } from '../helpers/util';

export const printer = (req, res) => {
  const maxNumber = req.query.maxNumber || 100;

  let data = [];
  for (let i = 1; i <= maxNumber; i++) {
    if (isDivisibleBy({ number: i, divider: 3 }) && isDivisibleBy({ number: i, divider: 5 })) {
      data.push('Visual Nuts');
    }
    else if (isDivisibleBy({ number: i, divider: 3 })) {
      data.push('Visual');
    }
    else if (isDivisibleBy({ number: i, divider: 5 })) {
      data.push('Nuts');
    }
    else {
      data.push(i)
    }
  }

  res.send(data);
};
