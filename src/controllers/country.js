import countries from '../mocks/countries';
import {
  getNumberOfCountries,
  getCountryWithMoreLanguages,
  getMostCommonLanguage
} from '../helpers/country';

export const data = (req, res) => {
  const numberOfCountries = getNumberOfCountries(countries);
  const countryWithMoreLanguages = getCountryWithMoreLanguages(countries);
  const mostCommonLanguages = getMostCommonLanguage(countries);

  res.send({
    countries: {
      total: numberOfCountries,
     withMoreLanguages: countryWithMoreLanguages,
    },
    languages: {
      mostCommon: mostCommonLanguages
    }
  });
};
