import express from 'express';
import { data } from '../controllers/country';

const router = express.Router();

router.get('/', data);

export default router;
