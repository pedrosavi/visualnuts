import express from 'express';
import { printer } from '../controllers/console';

const router = express.Router();

router.get('/', printer);

export default router;
