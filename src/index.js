import express from 'express';
import { } from 'dotenv/config';
import countryRoute from './routes/country';
import consoleRoute from './routes/console';

const PORT = process.env.PORT || 5000;
const app = express();

app.use(express.json());
app.use('/console', consoleRoute);
app.use('/country', countryRoute);

app.listen(PORT, () => {
    console.log(`Connected on port ${PORT}`);
});
