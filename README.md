# Visual Nuts

## Follow this steps to start the server

### 1 - Install dependencies

In the project directory, you can run:

### `npm install`

### 2 - Start the server

Run:

### `npm start`

## Making requests

### 1 - Exercice 1

In this exercice I don't put console.log to print the results.
You can see the result making a `GET` request to `http://localhost:5000/console` to see the result.

If you like, you can set the maximuim numbem using the parameter `maxNumber`.
Ex: `http://localhost:5000/console?maxNumber=350`

### 2 - Exercice 2

You can make a `GET` request to `http://localhost:5000/country` to see the result

## PS

I didn't make any unit test, because I never worked if that. I prefer to be transparent and I'm not shame with that, but I can learn fast any things that I'm not familiar with.

## Last words

I hope you enjoy it and allow me to be part fo the team
